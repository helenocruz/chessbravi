asgiref==3.4.1
backports.zoneinfo==0.2.1
coverage==6.2
Django==4.0
gunicorn==20.1.0
numpy==1.21.5
sqlparse==0.4.2
Werkzeug==2.0.2
