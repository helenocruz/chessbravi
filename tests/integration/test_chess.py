from datetime import datetime, timedelta
from http import HTTPStatus
from typing import List
from django.test import RequestFactory, TestCase
from django.test import Client
import json

class TestChessCase(TestCase):

    def test_add_game(self) -> None:
        c = Client(enforce_csrf_checks=True)
        response = c.post('/v1/game/new', {
                "name": "Game Test",
                "number_rows_columns": 8
            },
            content_type="application/json"
        )
        self.assertEqual(response.status_code, HTTPStatus.CREATED)


    def test_add_game_with_error(self) -> None:
        c = Client(enforce_csrf_checks=True)
        response = c.post('/v1/game/new', {
                "name": "Game Test",
            },
            content_type="application/json"
        )
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)


    def test_get_games(self) -> None:
        c = Client(enforce_csrf_checks=True)
        response = c.post('/v1/game/new', {
                "name": "Game Test",
                "number_rows_columns": 8
            },
            content_type="application/json"
        )

        c = Client()
        response = c.get('/v1/game/list', HTTP_ACCEPT='application/json')
        self.assertEqual(type(response.json()), list)


    def test_get_status_game(self) -> None:
        c = Client(enforce_csrf_checks=True)
        response = c.post('/v1/game/new', {
                "name": "Game Test",
                "number_rows_columns": 8
            },
            content_type="application/json"
        )

        c = Client()
        response = c.get('/v1/game/list', HTTP_ACCEPT='application/json')
        
        data_game = response.json()
        id_game = data_game[0]['id']

        response_status = c.get('/v1/game/status?id' + id_game, HTTP_ACCEPT='application/json')

        self.assertEqual(type(response_status.json()), dict)


    def test_get_status_game_without_id_will_return_error(self) -> None:
        c = Client()
        response_status = c.get('/v1/game/status', HTTP_ACCEPT='application/json')

        self.assertEqual(response_status.status_code, 400)