from datetime import datetime
from uuid import uuid4
from django.db import models


def get_uuid():
    return str(uuid4())


class Game(models.Model):
    id = models.CharField(max_length=100, default=get_uuid, primary_key=True)
    name = models.CharField(max_length=50)
    number_rows_columns = models.IntegerField(default=8)
    matrix_game = models.JSONField()
    updated_at = models.DateTimeField(default=datetime.now)
    created_at = models.DateTimeField(default=datetime.now)


class history(models.Model):
    id = models.CharField(max_length=100, default=get_uuid, primary_key=True)
    id_game = models.CharField(max_length=100)
    matrix_game_before = models.JSONField()
    piece = models.CharField(max_length=30)
    square_before = models.CharField(max_length=30)
    square_after = models.CharField(max_length=30)
    created_at = models.DateTimeField(default=datetime.now)