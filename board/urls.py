from django.urls import path
from . import views
from .v1.handlers import chess

urlpatterns = [
    path('', views.index, name='index'),
    path('v1/game/list', chess.get_all_games, name='list'),
    path('v1/game/new', chess.new, name='new'),
    path('v1/game/status', chess.get_status, name='get_status_game'),
    path('v1/game/addpiece', chess.add_piece, name='add_piece'),
    path('v1/game/movementpossiblepiece', chess.movement_possible_to_piece, name='movement_possible_to_piece'),
]