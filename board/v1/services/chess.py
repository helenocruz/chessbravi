from board.models import Game
import json
import numpy as np
from typing import Optional, Union
from werkzeug.exceptions import BadRequest
from board.v1.services.core_game import core_of_the_game


def get_games() -> list:
    games = Game.objects.all()
    
    list_games = [{
        "id": game.id,
        "name": game.name,
        "number_rows_columns": game.number_rows_columns
    } for game in games];
        
    return list_games


def get_status_game(id) -> Union[str, None]:
    game = Game.objects.filter(id = id).first()
    if game == None:
        return None
        
    matrix = json.loads(game.matrix_game)
    return matrix


def add_game(data) -> bool:
    parsed_data = _parse_add_game(data)
    if parsed_data ==  False:
        return False

    new_game = Game(
        name = parsed_data['name'],
        number_rows_columns = parsed_data['number_rows_columns'],
        matrix_game = json.dumps(np.zeros((parsed_data['number_rows_columns'], parsed_data['number_rows_columns']), dtype=str).tolist())
    )  
    new_game.save()
    return True


def save_game(id_game, matrix) -> bool:
    game = Game.objects.filter(id = id_game).first()
    if game == None:
        return None

    game.matrix_game = json.dumps(matrix)
    game.save()

    return True


def add_piece_in_the_board(data) -> Union[list, None]:
    data = _parse_add_piece_in_the_board(data)
    if data == False:
        return None

    matrix = get_status_game(data["game_id"])
    if matrix != None:
        board = core_of_the_game(matrix = matrix)
        new_board = board.add_piece(data["name_piece"], data["color"], data["horizontal_position"], data["vertical_position"])
        save_game(data["game_id"], new_board)
        return new_board

    return None


def get_possibles_movements_to_piece(id_game = None, horizontal_position = None, vertical_position = None):
    
    data_parsed = _parse_get_possibles_movements_to_piece({
        "horizontal_position": horizontal_position,
        "vertical_position": vertical_position,
        "game_id": id_game
    })

    matrix = get_status_game(id_game)
    board = core_of_the_game(matrix = matrix)
    
    movements = board.get_possibles_movements_to_piece(
        horizontal_position = data_parsed["horizontal_position"], 
        vertical_position = data_parsed["vertical_position"]
    )

    data_to_return = []
    for movement in movements:
        #data = [chr(movement[0] + 96 + 1), (movement[1] + 1)]
        key = chr(movement[0] + 96 + 1) + '-' + str(movement[1] + 1)

        moves_2_turn = board.get_possibles_movements_to_piece(
                        horizontal_position = movement[0], 
                        vertical_position = movement[1],
                        name_piece = "knight"
                    )

        data_2_turn = []
        for move in moves_2_turn:
            move_2_turn = chr(move[0] + 96 + 1) + '-' + str(move[1] + 1)
            data_2_turn.append(move_2_turn)

        data = {
            key: data_2_turn
        }

        data_to_return.append(data)

    return data_to_return


def _parse_add_game(data) -> dict:
    if "name" not in data or \
        "number_rows_columns" not in data:
        return False
    
    if type(data["name"]) != str:
        return False

    if type(data["number_rows_columns"]) != int:
        return False    

    if (data["number_rows_columns"] % 2) > 0:
        return False    

    parsed_data_to_return = {
        "name": data["name"],
        "number_rows_columns": data["number_rows_columns"],
    }

    return parsed_data_to_return


def _parse_add_piece_in_the_board(data) -> dict:
    if "game_id" not in data or \
        "horizontal_position" not in data or \
        "vertical_position" not in data or \
        "color" not in data or \
        "name_piece" not in data:
        BadRequest.detailed = "Has field missing in your request, check the documentation."
        raise(BadRequest)

    if type(data["game_id"]) != str:
        BadRequest.detailed = "The 'game_id' field is not valid, it must be a string."
        raise(BadRequest)

    if type(data["vertical_position"]) != int:
        BadRequest.detailed = "The 'vertical_position' field is not valid, it must be a integer."
        raise(BadRequest)

    if type(data["horizontal_position"]) != str:
        BadRequest.detailed = "The 'horizontal_position' field is not valid, it must be a string."
        raise(BadRequest)    

    if data["color"] != "black" and data["color"] != "white":
        BadRequest.detailed = "The 'color' field is not valid, it must be a value equal to 'black' or 'white'."
        raise(BadRequest)

    horizontal_position = 0
    if type(data["horizontal_position"]) == str:
        horizontal_position = (ord(data["horizontal_position"]) - 1) - 96

    vertical_position = data["vertical_position"] - 1

    parsed_data_to_return = {
        "game_id": data["game_id"],
        "name_piece": data["name_piece"],
        "horizontal_position": horizontal_position,
        "vertical_position": vertical_position,
        "color": data["color"],
    }

    return parsed_data_to_return


def _parse_get_possibles_movements_to_piece(data) -> dict:
    if "game_id" not in data or \
        "horizontal_position" not in data or \
        "vertical_position" not in data:
        BadRequest.detailed = "Has field missing in your request, check the documentation."
        raise(BadRequest)

    if type(data["game_id"]) != str:
        BadRequest.detailed = "The 'game_id' field is not valid, it must be a string."
        raise(BadRequest)

    if type(data["vertical_position"]) != int:
        BadRequest.detailed = "The 'vertical_position' field is not valid, it must be a integer."
        raise(BadRequest)

    if type(data["horizontal_position"]) != str:
        BadRequest.detailed = "The 'horizontal_position' field is not valid, it must be a string."
        raise(BadRequest)    
    
    horizontal_position = 0
    if type(data["horizontal_position"]) == str:
        horizontal_position = (ord(data["horizontal_position"]) - 1) - 96

    vertical_position = int(data["vertical_position"]) - 1

    parsed_data_to_return = {
        "game_id": data["game_id"],
        "horizontal_position": horizontal_position,
        "vertical_position": vertical_position,
    }

    return parsed_data_to_return

