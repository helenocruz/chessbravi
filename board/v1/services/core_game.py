from typing import Union
import numpy as np
from werkzeug.exceptions import BadRequest

class core_of_the_game:

    def __init__(self, matrix):
        self.matrix = np.array(matrix, dtype=object)

    
    def check_limit_of_the_board(self, horizontal_position_wished, vertical_position_wished):
        rows, cols = self.matrix.shape
        if rows > horizontal_position_wished and cols > vertical_position_wished and horizontal_position_wished >= 0 and vertical_position_wished >= 0:
            return True
            #need better this

        return False


    def get_pieces(self, name_piece = None, color = 'black', html_entity = None) -> Union[dict, bool]:

        box_of_white_pieces = {
            "king":"&#9812;",
            "queen":"&#9813;",
            "rock":"&#9814;",
            "bishop":"&#9815;",
            "knight":"&#9816;",
            "pawn":"&#9817;"
        }

        box_of_black_pieces = {
            "king":"&#9818;",
            "queen":"&#9819;",
            "rock":"&#9820;",
            "bishop":"&#9821;",
            "knight":"&#9822;",
            "pawn":"&#9823;"
        }

        if html_entity != None:
            for name, content in box_of_white_pieces.items():
                if content == html_entity:
                    return name

            for name, content in box_of_black_pieces.items():
                if content == html_entity:
                    return name

            return None
        
        if name_piece is None or (color != 'black' and color != 'white'):
            return False

        if color == "black":
            if name_piece in box_of_black_pieces:
                return box_of_black_pieces[name_piece]

        if color == "white":
            if name_piece in box_of_white_pieces:
                return box_of_white_pieces[name_piece]

        return False

    
    def verify_if_position_is_free(self, horizontal_position = None, vertical_position = None):
        matrix = self.matrix
        if matrix[vertical_position][horizontal_position] == "":
            return True
        
        return False


    def add_piece(self, name_piece = None, color = None, horizontal_position = None, vertical_position = None):
        vertical_position = (self.matrix.shape[0] - (vertical_position + 1)) 
        if self.check_limit_of_the_board(horizontal_position, vertical_position) == False:
            return self.matrix.tolist()

        if self.verify_if_position_is_free(horizontal_position, vertical_position) == True:
            self.matrix[vertical_position][horizontal_position] = str(self.get_pieces(name_piece, color))
            new_matrix = self.matrix
            return new_matrix.tolist()
        
        return self.matrix.tolist()


    def remove_piece_by_position(self, horizontal_position = None, vertical_position = None):
        if self.check_limit_of_the_board(horizontal_position, vertical_position) == False:
            return self.matrix.tolist()

        new_matrix = self.matrix
        new_matrix[vertical_position][horizontal_position] = str("")
        return new_matrix.tolist()

    
    def get_name_piece(self, horizontal_position = None, vertical_position = None):
        piece = self.matrix[int(vertical_position)][int(horizontal_position)]
        name_piece = self.get_pieces(html_entity = piece)
        return name_piece

    
    def get_possibles_movements_to_piece(self, horizontal_position = 0, vertical_position = 0, name_piece = None):
        possibles_movements = []
        vertical_position = (self.matrix.shape[0] - (int(vertical_position) + 1))
        if name_piece == None:
            name_piece = self.get_name_piece(horizontal_position = horizontal_position, vertical_position = vertical_position)
        
        if name_piece == "knight":
            possibles_movements = self._get_possibles_movement_to_knight(horizontal_position = horizontal_position, vertical_position = vertical_position)
            return possibles_movements
        
        BadRequest.detailed = "This feature is supported only for knight piece."
        raise(BadRequest)        
        

    def _get_possibles_movement_to_knight(self, horizontal_position = None, vertical_position = None):
        if horizontal_position == None or vertical_position == None:
            return False

        data_to_return = []
        possibilities = []

        possibilities.append([(int(vertical_position) + 2), (int(horizontal_position) - 1)])
        possibilities.append([(int(vertical_position) + 2), (int(horizontal_position) + 1)])
        possibilities.append([(int(vertical_position) - 2), (int(horizontal_position) - 1)])
        possibilities.append([(int(vertical_position) - 2), (int(horizontal_position) + 1)])
        possibilities.append([(int(vertical_position) - 1), (int(horizontal_position) + 2)])
        possibilities.append([(int(vertical_position) - 1), (int(horizontal_position) - 2)])
        possibilities.append([(int(vertical_position) + 1), (int(horizontal_position) - 2)])
        possibilities.append([(int(vertical_position) + 1), (int(horizontal_position) + 2)])

        for possibility in possibilities:
            if self.check_limit_of_the_board(possibility[0], possibility[1]) == True:
                data = [(possibility[1]), (self.matrix.shape[0] - possibility[0] - 1)]
                data_to_return.append(data)
            
        return data_to_return
        

