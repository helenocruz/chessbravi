from datetime import datetime
from http import HTTPStatus
import json
from typing import List
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from werkzeug.exceptions import BadRequest
from board.v1.services.chess import add_game, get_games, get_status_game, add_piece_in_the_board, get_possibles_movements_to_piece


@csrf_exempt
@require_http_methods(["GET"])
def get_all_games(request):
    try:
        data = get_games()
        if data != False:
            return JsonResponse(data, safe=False, status=HTTPStatus.OK)
        
        return JsonResponse({}, status=HTTPStatus.OK)

    except Exception as e:
        return JsonResponse({"message": "Internal server error."}, status=HTTPStatus.INTERNAL_SERVER_ERROR)


@csrf_exempt
@require_http_methods(["POST"])
def new(request):
    try:
        data = request.body.decode('utf-8')
        data = json.loads(data)
        if add_game(data) == True:
            return JsonResponse({}, status=HTTPStatus.CREATED)
        
        raise(BadRequest)

    except BadRequest as e:
        return JsonResponse({"message": "Problems validating request", "detailed": "Please, check the documentation."}, status=HTTPStatus.BAD_REQUEST)

    except Exception as e:
        return JsonResponse({"message": "Internal server error."}, status=HTTPStatus.INTERNAL_SERVER_ERROR)


@csrf_exempt
@require_http_methods(["GET"])
def get_status(request):
    try:
        id = request.GET.get("id")
        if id == None:
            raise(BadRequest)

        data = get_status_game(id)
        if data != None:
            return JsonResponse(data, safe=False, status=HTTPStatus.OK)
        
        raise(BadRequest)

    except BadRequest as e:
        return JsonResponse({"message": "Problems validating request", "detailed": "Please, check the documentation."}, status=HTTPStatus.BAD_REQUEST)

    except Exception as e:
        return JsonResponse({"message": "Internal server error."}, status=HTTPStatus.INTERNAL_SERVER_ERROR)


@csrf_exempt
@require_http_methods(["POST"])
def add_piece(request):
    try:
        data = request.body.decode('utf-8')
        data = json.loads(data)

        matrix_result = add_piece_in_the_board(data)
        if type(matrix_result) == list:
            return JsonResponse(matrix_result, safe=False, status=HTTPStatus.CREATED)
        
        raise(BadRequest)

    except BadRequest as e:
        return JsonResponse({"message": "Problems validating request", "detailed": e.detailed}, status=HTTPStatus.BAD_REQUEST)

    except Exception as e:
        return JsonResponse({"message": "Internal server error."}, status=HTTPStatus.INTERNAL_SERVER_ERROR)


@csrf_exempt
@require_http_methods(["GET"])
def movement_possible_to_piece(request):
    try:
        id = request.GET.get("id_game")
        horizontal_position = request.GET.get("horizontal_position")
        vertical_position = request.GET.get("vertical_position")

        result = get_possibles_movements_to_piece(
            id_game = id,
            horizontal_position = horizontal_position,
            vertical_position = int(vertical_position),
        )
    
        return JsonResponse(result, safe=False, status=HTTPStatus.OK)
        
    except BadRequest as e:
        return JsonResponse({"message": "Problems validating request", "detailed": e.detailed}, status=HTTPStatus.BAD_REQUEST)

    except Exception as e:
        return JsonResponse({"message": "Internal server error."}, status=HTTPStatus.INTERNAL_SERVER_ERROR)
