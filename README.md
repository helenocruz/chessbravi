# CHESS BRAVI
This service is responsible for coordinating the chess game.

# Live demo
See the live demo [here](http://191.252.201.79:8000/)

## Environment
It's required python3.7+ and pip latest version.

### Install dependencies
```bash
pip install -r requirements.txt
pip install black
pip install coverage
```

### Run
Run the App:
```bash
make dev
```

### Tests
We use [coverage](https://pypi.org/project/coverage/) to check code coverage. To run tests just execute command below:
```bash
make test
```

### Migrations
All models are at `board.models`. They can be freely edited but for the changes take place at
database two commmands are needed.

**Create migration**
```bash
make makemigration
```

**Apply migrations into database**
```bash
make migrate
```

## Endpoints

### List all Posts that is in monitoring.
**GET** /{version}/game/status

#### Query Params
Param | Type | Description 
--- | --- | ---
id | String | The ID of the Game that you wish to get the status about the chessboard (Matrix).

#### Response Schema
```yaml
    type: List of array!
```

Example address: /v1/game/status?id=d97c7c5a-fa33-4bc4-afcd-3d664eb9e059

#### Response Status
Status Code | Description
--- | ---
200 | Fetched successfully
404 | Validation error
500 | Internal server error


### List all Posts that is in monitoring.
**GET** /{version}/game/list

#### Query Params
No has params.

Example address: /v1/game/list

#### Response Schema
```yaml
    properties:
        id: 
            type: String!
        name: 
            type: String!
        number_rows_columns: 
            type: Integer!
```

#### Response Status
Status Code | Description
--- | ---
200 | Fetched successfully
404 | Validation error
500 | Internal server error


### Add New Game
**POST** /{version}/game/new

#### Payload
```yaml
properties:
    name: 
        type: String!
    number_rows_columns: 
        type: Integer!
```

The field number_rows_columns needs to be a even number. Look at the example below:

```yaml
    {
        "name": "Master Chess",
        "number_rows_columns": 12
    }
```

#### Response Status
Status Code | Description
--- | ---
201 | Post inserted successfully
400 | Validation error
500 | Internal server error


### Get all possible movements for a piece 
**GET** /{version}/game/movementpossiblepiece

This feature is available just only for the knight piece of the chess, for now.

#### Payload
```yaml
properties:
    game_id: 
        type: String!
    horizontal_position: 
        type: String!
    vertical_postion: 
        type: Integer!
```

##### Example
Address: /v1/game/movementpossiblepiece?id_game=73941cbe-fefe-4e46-bc3b-2aae7c1f6a8d&vertical_position=1&horizontal_position=b

#### Response Schema
```yaml
properties:
    type: List!
```

#### Response Status
Status Code | Description
--- | ---
200 | Post inserted successfully
400 | Validation error
500 | Internal server error
