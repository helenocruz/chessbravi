citest:
	make test
	coverage xml

dev:
	python manage.py runserver

makemigrations:
	python manage.py makemigrations

migrate:
	python manage.py migrate

run:
	gunicorn --bind :8000 --workers 1 --threads 8 --timeout 0 manage

test:
	coverage run --source='.' manage.py test tests
	coverage html
	coverage report

test2:
	coverage run -m unittest
	coverage html
	coverage report