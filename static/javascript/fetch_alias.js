const serverAddress = 'http://localhost:8000';

function getAddressServer() {
    return serverAddress;
}

async function post_fetch(url = '', data = {}) {
    const response = await fetch(serverAddress + url, {
        method: 'POST', 
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin', 
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow', 
        referrer: 'no-referrer',
        body: JSON.stringify(data),
    });
    return await response;
}

async function get_fetch(url = '') {
    const response = await fetch(serverAddress + url);
    return response;
}