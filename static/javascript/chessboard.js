var _id_game_selected = null;


async function init() {
    get_fetch('/v1/game/list').then((response)=>{ 
        response.json().then((result)=>{
            document.getElementById("games_list").innerHTML = "";

            let opt = document.createElement("option");
            opt.text = "";
            document.getElementById("games_list").options.add(opt);

            if(result.length == 0){
                alert_messagem('alert_games_list', 'danger', 'You wil need to add a new game.');
                return;
            }
            result.forEach((e,i)=>{
                let opt = document.createElement("option");
                opt.text = e.name;
                opt.value = e.id;
                document.getElementById("games_list").options.add(opt);
            });
        });
    });
}


function selectGame(id_game){
    _id_game_selected = id_game;
    document.getElementById("vertical_position").innerHTML = "";
    document.getElementById("horizontal_position").innerHTML = "";

    get_fetch('/v1/game/status?id=' + id_game).then((response)=>{ 
        response.json().then((result)=>{
            drawChessBoard('board', result);

            for(var i=1; i<=result.length; i++){
                let opt = document.createElement("option");
                opt.text = i;
                opt.value = i;
                document.getElementById("vertical_position").options.add(opt);
                
                let opt_horizontal = document.createElement("option");
                opt_horizontal.text = String.fromCharCode(97 + (i - 1));
                opt_horizontal.value = String.fromCharCode(97 + (i - 1));
                document.getElementById("horizontal_position").options.add(opt_horizontal);
            }

        });
    });

}


function drawChessBoard(id_div_target = 'board', matrix = null){

    if(id_div_target == null || matrix == null) return false;
    var target = document.getElementById(id_div_target);

    let length_board = matrix.length + 2;

    let chess_html = '<div style="padding-left: 20px;"><div class="chessboard" style="width: ' + ((length_board)*60 + 20) + 'px; height: ' + ((length_board)*60 + 20) + 'px">';
    style_squade = 'white';

    number_of_squad_in_the_board = length_board*length_board;
    
    for(i=0; i<number_of_squad_in_the_board; i++){
        
        line = parseInt(i / length_board);
        column = (i - ((line)*length_board));

        let id_for_this_element = "square_" + (length_board - line - 1) + "_" + String.fromCharCode(column + 96);


        if(column == 0 || column == (line)*length_board || parseInt(line) > (length_board - 2) || column == (length_board-1) || line == 0){
            if((line == 0 && column == 0) || (line == 0 && column == (length_board-1)) || (line == (length_board-1) && column == (length_board-1)) || (line == (length_board-1) && column == 0))
                chess_html += '<div class="tag" id=""></div>';
            else if(line == 0 || line == (length_board - 1))
                chess_html += '<div class="tag" id="">' + String.fromCharCode(column + 96) + '</div>';
            else
                chess_html += '<div class="tag" id="">' + ((length_board - 1) - line) + '</div>';

        } else {
            chess_html += '<div class="' + style_squade + '" id="' + id_for_this_element + '" onClick="click_square(' + (length_board - line - 1) + ',\'' + String.fromCharCode(column + 96) + '\')">' + matrix[(line-1)][(column-1)] + '</div>';
        }   
        if((column + 1) != length_board)
            style_squade = (style_squade == 'white') ? 'black' : 'white';
    }

    chess_html += '</div>';

    target.innerHTML = chess_html;
}


function click_square(line, column){
    document.getElementById("alert_message_board").innerHTML = ""
    get_fetch('/v1/game/movementpossiblepiece?id_game=' + _id_game_selected + '&vertical_position=' + line + '&horizontal_position=' + column)
    .then((response)=>{ 
        if(response.status == 400){
            alert("The feature to show possible movements (first and second turn) is working just only \"knight piece\". Click on the \"knight piece\".")
            alert_messagem('alert_message_board', 'danger', 'The feature to show possible movements (first and second turn) is working just only \"knight piece\". Click on the \"knight piece\".');
            return;
        }
        
        response.json()
        .then((result)=>{
            let html_message = "";
            result.forEach((e,i)=>{
                html_message += "<br /><div><b>Option " + (i+1) + " to first and next movement: </b>";
                html_message += "<div>" + JSON.stringify(e) +  "</div>";  
            });
            html_message += "</div>"
            alert_messagem('alert_message_board', 'danger', html_message);           
        });
    });
}


function alert_messagem(div_parent, type_message, message){
    var alert_div = document.getElementById(div_parent)
    var wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-' + type_message + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'
    alert_div.append(wrapper)
}


function create_new_game(){    
    let name_new_game = document.getElementById('name_new_game').value;
    let number_rows_columns = document.getElementById('number_rows_columns_new_game').value;
    
    if(name_new_game.length < 4){
        alert_messagem('alert_add_new_game', 'danger', 'The name of the game needs to have at least 4 characters.');
        return;
    }

    if(parseInt(number_rows_columns) < 8 || parseInt(number_rows_columns) > 14){
        alert_messagem('alert_add_new_game', 'danger', 'The number of rows needs to be between 8 and 16 characters.');
        return;
    }

    if((parseInt(number_rows_columns)%2) > 0){
        alert_messagem('alert_add_new_game', 'danger', 'The number of rows needs to be a pair.');
        return;
    }

    let data_to_write = {
        name: name_new_game,
        number_rows_columns: parseInt(number_rows_columns)
    }

    post_fetch("/v1/game/new", data_to_write)
        .then((data)=>{
            if(data.status == 201) {
                alert_messagem('alert_add_new_game', 'success', 'New game "' + name_new_game + '" was added successfully.');
                document.getElementById('name_new_game').value = "";
                init();            
            } else {
                alert_messagem('alert_add_new_game', 'danger', 'Internal server error. Try again later!');
            }
        }
    )
}


function add_piece(){
    if(_id_game_selected == null){ 
        alert_messagem('alert_add_piece', 'danger', 'You need to select a game first!');
        return;
    }

    if(document.getElementById('horizontal_position').value == "" || document.getElementById('vertical_position').value == ""){ 
        alert_messagem('alert_add_piece', 'danger', 'Exist empty field. Try again!');
        return;
    }

    let data = {
        "game_id": _id_game_selected,
        "name_piece": document.getElementById('name_piece').value,
        "color": document.getElementById('piece_color').value,
        "horizontal_position": document.getElementById('horizontal_position').value,
        "vertical_position": parseInt(document.getElementById('vertical_position').value)
    }

    post_fetch("/v1/game/addpiece", data)
        .then((response)=>{
            if(response.status == 201) {
                response.json().then((result)=>{
                    drawChessBoard('board', result);
                    alert_messagem('alert_add_piece', 'success', 'New piece "' + data["name_piece"] + '" was added successfully.');
                });
            } else {
                alert_messagem('alert_add_piece', 'danger', 'Internal server error. Try again later!');
            }
        }
    );

}
